package Semana3;

public class Pelicula {
    private String codigo;
    private String nombre;
    private String director;
    private int copias;

    public Pelicula() {
    }

    public Pelicula(String codigo, String nombre, String director, int copias) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.director = director;
        this.copias = copias;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getCopias() {
        return copias;
    }

    public void setCopias(int copias) {
        this.copias = copias;
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo +
                ", Nombre: " + nombre +
                ", director: " + director +
                ", copias: " + copias;
    }
}
