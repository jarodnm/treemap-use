package Semana3;

public class Empleado {

    private String identificacion;
    private String nombre;
    private String apellidos;
    private String correo;
    private String clave;

    public Empleado(){

    }

    public Empleado(String identificacion, String nombre, String apellidos, String correo, String clave) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correo = correo;
        this.clave = clave;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return "Identificacion: " + identificacion+
                ", nombre: " + nombre+
                ", apellidos: " + apellidos+
                ", correo: " + correo+
                ", clave: " + clave;
    }
}

