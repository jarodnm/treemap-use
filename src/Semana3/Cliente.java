package Semana3;

public class Cliente {

    private String codigo;
    private String identificacion;
    private String nombre;
    private String apellido;
    private String correo;
    private String clave;
    private String direccion;
    private String telefono;

    public Cliente() {

    }

    public Cliente(String codigo, String identificacion, String nombre, String apellido, String correo, String clave, String direccion, String telefono) {
        this.codigo = codigo;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.clave = clave;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo +
                ", identificacion: " + identificacion +
                ", nombre: " + nombre +
                ", apellido: " + apellido+
                ", correo: " + correo +
                ", clave: " + clave+
                ", direccion: " + direccion  +
                ", telefono: " + telefono  ;

    }
}
