package Semana3;


public class Controller {
    CL capa = new CL();

    public Controller(){

    }

    public void registrarEmpleado(String pidentificacion,String pnombre,String papellidos,String pcorreo,String pclave){
        Empleado tmpEmpleado;
        tmpEmpleado = new Empleado(pidentificacion,pnombre,papellidos,pcorreo,pclave);
        capa.registrarEmpleado(pidentificacion,tmpEmpleado);
    }

    public String[] listarEmpleados(){
        return capa.getEmpleados();
    }

    public boolean validarEmpleado(String pid){
        return capa.empleadoExiste(pid);
    }

    public String[] listarClientes(){
        return capa.getClientes();
    }

    public void registrarCliente(String pcodigo, String pidentificacion, String pnombre, String papellido, String pcorreo, String pclave, String pdireccion, String ptelefono){
        Cliente tmpcliente;
        tmpcliente = new Cliente(pcodigo,pidentificacion,pnombre,papellido,pcorreo,pclave,pdireccion,ptelefono);
        capa.registrarCliente(pcodigo,tmpcliente);
    }

    public boolean validarCliente(String pcodigo){
        return capa.clienteExiste(pcodigo);
    }

    public void registrarPelicula(String pcodigo, String pnombre, String pdirector, int pcopias){
        Pelicula tmppelicula;
        tmppelicula = new Pelicula(pcodigo,pnombre,pdirector,pcopias);
        capa.registrarPelicula(pcodigo,tmppelicula);
    }

    public String[] listarPeliculas(){
        return capa.getPeliculas();
    }

    public boolean validarPelicula(String pcodigo){
        return capa.peliculaExiste(pcodigo);
    }
}
