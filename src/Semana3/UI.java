package Semana3;

import java.io.*;


public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        int opcion = 0;
        do {
            menu();
            opcion = procesarOpcion(opcion);
            noSalir = ejecutarAccion(opcion);
        } while (noSalir);

    }

    static void menu() throws IOException {
        out.println("|--------------------------|");
        out.println("|Digite la opcion que guste|");
        out.println("|--------------------------|");
        out.println("| 1. Registrar empleado    |");
        out.println("|--------------------------|");
        out.println("| 2. Listar empleado       |");
        out.println("|--------------------------|");
        out.println("| 3. Registrar cliente     |");
        out.println("|--------------------------|");
        out.println("| 4. Listar cliente        |");
        out.println("|--------------------------|");
        out.println("| 5. Registrar pelicula    |");
        out.println("|--------------------------|");
        out.println("| 6. Listar pelicula       |");
        out.println("|--------------------------|");
        out.println("| 7. Salir                 |");
        out.println("|__________________________|");
    }

    static int procesarOpcion(int popcion) throws IOException {
        out.println("Digite su opcion");
        popcion = Integer.parseInt(in.readLine());
        return popcion;
    }

    static boolean ejecutarAccion(int popcion) throws IOException {
        boolean noSalir = true;
        switch (popcion) {
            case 1:
                registrarEmpleado();
                break;
            case 2:
                listarEmpleados();
                break;
            case 3:
                registrarCliente();
                break;
            case 4:
                listarClientes();
                break;
            case 5:
                registrarPelicula();
                break;
            case 6:
                listarPeliculas();
                break;
            case 7:
                out.println("Gracias por usar el programa");
                noSalir = false;
                break;
            default:
                out.println("Digite una opcion correcta");
        }

        return noSalir;
    }

    public static void registrarEmpleado() throws IOException {
        String identificacion;
        String nombre;
        String apellidos;
        String correo;
        String clave;

        out.println("Digite la identificación");
        identificacion = in.readLine();
        if(!gestor.validarEmpleado(identificacion)){
            out.println("Digite el nombre");
            nombre = in.readLine();
            out.println("Digite los apellidos");
            apellidos = in.readLine();
            out.println("Digite el correo");
            correo = in.readLine();
            out.println("Digite la clave");
            clave = in.readLine();

            gestor.registrarEmpleado(identificacion, nombre, apellidos, correo, clave);
        }else {
            out.println("El empleado ya fue registrado previamente");
        }
    }

    public static void registrarCliente() throws IOException {
        String codigo;
        String identificacion;
        String nombre;
        String apellido;
        String correo;
        String clave;
        String direccion;
        String telefono;

        out.println("Digite el código");
        codigo = in.readLine();
        if(!gestor.validarCliente(codigo)){
            out.println("Digite la identificación");
            identificacion = in.readLine();
            out.println("Digite el nombre");
            nombre = in.readLine();
            out.println("Digite el apellido");
            apellido = in.readLine();
            out.println("Digite el correo");
            correo = in.readLine();
            out.println("Digite la clave");
            clave = in.readLine();
            out.println("Digite la dirección");
            direccion = in.readLine();
            out.println("Digite el telefono");
            telefono = in.readLine();

            gestor.registrarCliente(codigo, identificacion, nombre, apellido, correo, clave, direccion, telefono);
        }else{
            out.println("El cliente ya fue registrado previamente");
        }

    }

    public static void registrarPelicula()throws IOException{
        String codigo;
        String nombre;
        String director;
        int copias;

        out.println("Digite el codigo de la pelicula");
        codigo = in.readLine();
        if(!gestor.validarPelicula(codigo)){
            out.println("Digite el nombre de la pelicula");
            nombre = in.readLine();
            out.println("Digite el nombre del director");
            director = in.readLine();
            out.println("Digite el numero de copias de esta pelicula");
            copias = Integer.parseInt(in.readLine());
            gestor.registrarPelicula(codigo,nombre,director,copias);
        }else{
            out.println("La pelicula ya fue registrada previamente");
        }
    }

    public static void listarEmpleados() throws IOException {
        for (String dato : gestor.listarEmpleados()) {
            out.println(dato);
        }
    }

    public static void listarClientes() throws IOException {
        for (String dato : gestor.listarClientes()) {
            out.println(dato);
        }
    }

    public static void listarPeliculas()throws IOException{
        for(String dato : gestor.listarPeliculas()){
            out.println(dato);
        }
    }

}
