package Semana3;

import java.util.*;

public class CL {
    static TreeMap<String,Empleado> empleados = new TreeMap<String, Empleado>();
    static TreeMap<String,Cliente> clientes = new TreeMap<String, Cliente>();
    static TreeMap<String,Pelicula> peliculas = new TreeMap<String, Pelicula>();

    public void registrarEmpleado(String id, Empleado tmpempleado) {
        empleados.put(id, tmpempleado);
    }

    public String[] getEmpleados() {
        String []datos = new String[empleados.size()];
        int pos = 0;
        for(Map.Entry<String,Empleado> dato : empleados.entrySet()){
        String valor = dato.getKey();
        datos[pos] = empleados.get(valor).toString();
        pos++;
     }
        return datos;
    }

    public void registrarPelicula(String codigo,Pelicula tmppelicula) {
        peliculas.put(codigo, tmppelicula);
    }

    public String[] getPeliculas() {
        String[] data = new String[peliculas.size()];
        int pos = 0;
        for(Map.Entry<String,Pelicula>dato:peliculas.entrySet()){
            String key = dato.getKey();
            data[pos] = peliculas.get(key).toString();
            pos++;
        }
        return data;
    }

    public String[] getClientes() {
        String[] data = new String[clientes.size()];
        int pos = 0;
        for(Map.Entry<String,Cliente>dato:clientes.entrySet()){
            String key = dato.getKey();
            data[pos] = clientes.get(key).toString();
            pos++;
        }
        return data;
    }

    public void registrarCliente(String codigo, Cliente tmpcliente) {
        clientes.put(codigo,tmpcliente);
    }

    public boolean empleadoExiste(String id){
        boolean existe = false;
       for(Map.Entry<String,Empleado>dato: empleados.entrySet()){
           String key = dato.getKey();
           if(empleados.get(key).getIdentificacion().equals(id)){
               existe = true;
           }
       }
       return existe;
    }

    public boolean clienteExiste(String codigo){
        boolean existe = false;
        for(Map.Entry<String,Cliente>dato: clientes.entrySet()){
            String key = dato.getKey();
            if(clientes.get(key).getCodigo().equals(codigo)){
                existe = true;
            }
        }
        return existe;
    }

    public boolean peliculaExiste(String codigo){
        boolean existe = false;
        for(Map.Entry<String,Pelicula>dato: peliculas.entrySet()){
            String key = dato.getKey();
            if(peliculas.get(key).getCodigo().equals(codigo)){
                existe = true;
            }
        }
        return existe;
    }



}
